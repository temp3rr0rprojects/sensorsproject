//#include <iostream>

//using namespace std;
class Camera
{
	private:
		string name;
		string stamp;
		int doTakeScreenshot(void)
		{
			try
			{		
				int i;
				if(system(NULL))
				{
					//string command = "";
					string command = "/opt/vc/bin/raspistill -awb auto -sh 50 -e bmp -w 1024 -h 768 -hf -r -q 100 -o /mnt/alarmpi_share/SensorsProject/bin/screenShots/";
					command.append(stamp);
					command.append(".jpg");
					i = system(command.c_str());

				}
			}
			catch(...)
			{

			}
			return 0;
		}
	public:
		Camera();
		
		int getScreenshot(void)
		{
			return doTakeScreenshot();
		}
		string getName(void)
		{
			return name;
		}
		void setStamp(string _inputStr)
		{
			stamp = _inputStr;
		}
};		

Camera::Camera()
{
	name = "Raspi Camera";
	stamp = "default";
}
