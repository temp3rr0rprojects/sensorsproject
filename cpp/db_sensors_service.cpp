#include <string>
#include <iostream>
#include <thread>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "Sensor.h"
#include "TempOneWire.h"
#include "I2C.cpp"
#include "AnalogSensor.h"
#include "DhtSensor.h"
#include "FlowSensor.h"
#include "DbController.h"
#include "ThermalZone.h"
#include "CpuFrequency.h"
#include "QueryLogger.h"
#include "TextLogger.h"

#define SERVICE     1
#define DEBUG       0
#define TEXT_LOG	0

#define LOG_INTERVAL	1800	// Seconds

using namespace std;

static volatile int flowCounter = 0;
static volatile int flowPulses = 0;

void flowInterrupt(void) {
    ++flowCounter;
}

void flowLogThread(void) {
	int pulses = 0;
    wiringPiSetup();
    wiringPiISR(0, INT_EDGE_FALLING, &flowInterrupt);
    std:thread flowThread(flowInterrupt);
    flowThread.join();
    
    auto start = chrono::system_clock::now();
    sleep(1);
    pulses = flowCounter;
    auto end = chrono::system_clock::now();
    flowCounter = 0;

    auto diff = end - start;
    float flDiff = chrono::duration<double, milli>(diff).count();
    float hz = pulses / (flDiff / 1000);
    float liters = pulses;
    liters /= 7.5;
    liters /= 60.0;
	flowPulses = pulses;
}

void Init(void) {
    system("/home/madks/Documents/CronJobs/modprobe.sh");
    system("/home/madks/Documents/CronJobs/one-wire.sh");
    system("/home/madks/Documents/CronJobs/i2c.sh");
}

void getFlowReading(void) {
	// Get Flow reading
	thread flowThread(flowLogThread);
	flowThread.join();
}

void makeService(void) {
	// Our process ID and Session ID
	pid_t pid, sid;
		
	// Fork off the parent process
	pid = fork();
	if (pid < 0)
		exit(EXIT_FAILURE);
	
	// If we got a good PID, then we can exit the parent process
	if (pid > 0)
		exit(EXIT_SUCCESS);

	// Change the file mode mask
	umask(0);
		   
	// Create a new SID for the child process
	sid = setsid();
	if (sid < 0) {
			// Log the failure
			exit(EXIT_FAILURE);
	}
	
	// Change the current working directory
	if ((chdir("/")) < 0) {
			// Log the failure
			exit(EXIT_FAILURE);
	}
	
	// Close out the standard file descriptors
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	// Daemon-specific initialization goes here
}

int main(void)
{
	if (SERVICE)
		makeService();
	
	// Load mods/libraries and start the flow thread	
    Init();
	
	while(1) {    
		// Instantiate Sensors
		TempOneWire digitalTemp;
		I2C mpl;
		AnalogSensor logTemp;
		DhtSensor dht;    
		FlowSensor fl;
		ThermalZone thermal;
		CpuFrequency cpuFreq;		
	
		// Instantiate Controllers
		DbController myController;	
		QueryLogger *ql;
		TextLogger *tx;

		// Instantiate sensors vector, flow sensor and the loggers
		ql = QueryLogger::getInstance();
		tx = TextLogger::getInstance();

		// Declare custom type vectors
	
		if(TEXT_LOG)
			tx->log("Init.");

		getFlowReading();	
		
		fl.setFlow(flowPulses);
		if (DEBUG) {
			cout << "Flow = " << fl.getFlow() << endl;
			cout << "Log temp = " << logTemp.getValue() << endl;
			cout << "Digital Temp: " << digitalTemp.getTemp() << endl;
			cout << "Cpu frequency: " << cpuFreq.getCpuFreq() << endl;
			cout << "Thermal zone: " << thermal.getThermalZone() << endl;
			cout << "I2c pressure: " << mpl.getPressure() << endl;
			cout << "I2c temperature: " << mpl.getTemperature() << endl;
			
			for (int i = 0; i < 8; i++)
				cout << "Analog sensor " << i << ": " << logTemp.getAdc(i) << endl;
			
			cout << "DHT temp: " << dht.getTemp() << endl;
			cout << "DHT humidity: " << dht.getHum() << endl;
		}	
		string insertString;

		insertString = "INSERT INTO \"log\" (aquatemp, dht11temp, dht11hum, flowpulses, cpufreq, mpltemp, mplpressure, thermalzone, analog0, analog1, analog2, analog3, analog4, analog5, analog6, analog7, created) VALUES (";
		insertString.append(to_string(digitalTemp.getTemp()));
		insertString.append(", ");
		insertString.append(to_string(dht.getTemp()));
		insertString.append(", ");
		insertString.append(to_string(dht.getHum()));
		insertString.append(", ");
		insertString.append(to_string(fl.getFlow()));
		insertString.append(", ");
		insertString.append(to_string(cpuFreq.getCpuFreq()));
		insertString.append(", ");
		insertString.append(to_string(mpl.getTemperature()));
		insertString.append(", ");
		insertString.append(to_string(mpl.getPressure()));
		insertString.append(", ");
		insertString.append(to_string(thermal.getThermalZone()));
		insertString.append(", ");
		insertString.append(to_string(logTemp.getAdc(0)));
		insertString.append(", ");
		insertString.append(to_string(logTemp.getAdc(1)));
		insertString.append(", ");
		insertString.append(to_string(logTemp.getAdc(2)));
		insertString.append(", ");
		insertString.append(to_string(logTemp.getAdc(3)));
		insertString.append(", ");
		insertString.append(to_string(logTemp.getAdc(4)));
		insertString.append(", ");
		insertString.append(to_string(logTemp.getAdc(5)));
		insertString.append(", ");
		insertString.append(to_string(logTemp.getAdc(6)));
		insertString.append(", ");
		insertString.append(to_string(logTemp.getAdc(7)));
		insertString.append(", NOW());");
			
		if(myController.connect())
			myController.execCommandInsert(insertString);
			
		sleep(LOG_INTERVAL);
	}
	exit(EXIT_SUCCESS);
}	
