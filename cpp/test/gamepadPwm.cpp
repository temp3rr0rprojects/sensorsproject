#include <string>
#include <thread>

#include <iostream>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <linux/joystick.h>

#include <wiringPi.h>
#include <softPwm.h>

using namespace std;

#define NAME_LENGTH 128

#define BUFSIZE 1024

#define SERVICE     0

int gArgc = 0;
char **gArgv;

/* 
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(0);
}

void makeService(void) {
	// Our process ID and Session ID
	pid_t pid, sid;
		
	// Fork off the parent process
	pid = fork();
	if (pid < 0)
		exit(EXIT_FAILURE);
	
	// If we got a good PID, then we can exit the parent process
	if (pid > 0)
		exit(EXIT_SUCCESS);

	// Change the file mode mask
	umask(0);
		   
	// Create a new SID for the child process
	sid = setsid();
	if (sid < 0) {
			// Log the failure
			exit(EXIT_FAILURE);
	}
	
	// Change the current working directory
	if ((chdir("/")) < 0) {
			// Log the failure
			exit(EXIT_FAILURE);
	}
	
	// Close out the standard file descriptors
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	// Daemon-specific initialization goes here
}

void moveMotors(int type, int button, int value) {
	//(js.type == 2 && js.value == 0 && js.number == 0) {
	
	int speed = 0;
	
	if (type == 2 && value < 0 && button == 1) {	
		// Front 
		speed = (-value / 320);
		
		softPwmWrite(0, speed);
		delay(10);
		
// 		softPwmWrite(0, 0);
// 		softPwmWrite(2, 0);
	} else 	if (type == 2 && value > 0 && button == 1) {	
		// Back Right
		speed = (value / 320);
		
		softPwmWrite(1, speed);
		delay(10);

// 		softPwmWrite(1, 0);
// 		softPwmWrite(3, 0);
	} else 	if (type == 2 && value < 0 && button == 3) {	
		//  Right
		speed = (-value / 320);

		softPwmWrite(2, speed);
 		delay(10);

// 		softPwmWrite(0, 0);
	} else 	if (type == 2 && value > 0 && button == 3) {	
		// Left
		speed = (value / 320);
		
		softPwmWrite(3, speed);

		delay(10);
// 		softPwmWrite(2, 0);
	} else 	if ((type == 2 && value == 0 && (button == 0 || button == 2)) || button == 14) {	
		// Center
		softPwmWrite(0, 0);
		softPwmWrite(1, 0);
		softPwmWrite(2, 0);
		softPwmWrite(3, 0);
		delay(10);
	}
}

int main(int argc, char **argv) {

	int fd;
	unsigned char axes = 2;
	unsigned char buttons = 2;
	int version = 0x000800;
	char name[NAME_LENGTH] = "Unknown";
	gArgc = argc;
	gArgv = argv;

	if ((fd = open("/dev/input/js0", O_RDONLY)) < 0) {
		perror("jsValue");
		exit(1);
	}
	
	wiringPiSetup();

	if (!((softPwmCreate(0, 0, 100) == 0) && softPwmCreate(1, 0, 100) == 0 && softPwmCreate(2, 0, 100) == 0 && softPwmCreate(3, 0, 100) == 0)) {
		perror("softwarePwm");
		exit(1);
	}
	
	ioctl(fd, JSIOCGVERSION, &version);
	ioctl(fd, JSIOCGAXES, &axes);
	ioctl(fd, JSIOCGBUTTONS, &buttons);
	ioctl(fd, JSIOCGNAME(NAME_LENGTH), name);

	printf("Joystick (%s) has %d axes and %d buttons. Driver version is %d.%d.%d.\n",
		name, axes, buttons, version >> 16, (version >> 8) & 0xff, version & 0xff);
	printf("Testing ... (interrupt to exit)\n");

	struct js_event js;
	
	if (SERVICE)
		makeService();	
	
	// Non Blocking mode
	while (1) {
		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			perror("\njsValue: error reading");
			exit (1);
		}
		
		moveMotors(js.type, js.number, js.value);				
	}

	return -1;
}