#include <sys/ioctl.h>
//#include <sys/time.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <linux/joystick.h>

#include <wiringPi.h>

#define NAME_LENGTH 128

int main (void)
{
	int fd;
	unsigned char axes = 2;
	unsigned char buttons = 2;
	int version = 0x000800;
	char name[NAME_LENGTH] = "Unknown";

	if ((fd = open("/dev/input/js0", O_RDONLY)) < 0) {
		perror("jstest");
		exit(1);
	}
	ioctl(fd, JSIOCGVERSION, &version);
	ioctl(fd, JSIOCGAXES, &axes);
	ioctl(fd, JSIOCGBUTTONS, &buttons);
	ioctl(fd, JSIOCGNAME(NAME_LENGTH), name);

	printf("Joystick (%s) has %d axes and %d buttons. Driver version is %d.%d.%d.\n",
		name, axes, buttons, version >> 16, (version >> 8) & 0xff, version & 0xff);
	printf("Testing ... (interrupt to exit)\n");

	struct js_event js;

	wiringPiSetup () ;
	pinMode (0, OUTPUT) ;

	while (1) {
		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			perror("\njstest: error reading");
			exit (1);
		}

// 		printf("Event: type %d, time %d, number %d, value %d\n",
// 			js.type, js.time, js.number, js.value);

		if (js.type == 1 && js.value == 1 && js.number == 9) {
			printf("Pressed button R2\n");
			
			// pew pew
			digitalWrite (0, HIGH);
			delay (100) ;
			digitalWrite (0,  LOW);
		}
	}
	return -1;
}
