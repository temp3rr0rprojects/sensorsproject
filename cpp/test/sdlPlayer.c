#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <smpeg/smpeg.h>
#include <dirent.h>

// Compile using:
// gcc `sdl-config --cflags --libs` sample.c -o sample -lSDL_mixer -lsmpeg

int main(int argc, char * argv[])
{
  SDL_Surface * screen;
  SMPEG *mpeg;
  SMPEG_Info info;

  /* Init SDL: */

  SDL_Init(SDL_INIT_VIDEO |
           SDL_INIT_AUDIO);

  mpeg = SMPEG_new("13._cave.mp3.mp3",&info, 1);

  SMPEG_play(mpeg);

  do
    {
      SDL_Delay(250);
    }while(!SDL_QuitRequested() && SMPEG_status(mpeg)==SMPEG_PLAYING);

  SMPEG_delete(mpeg);
  SDL_Quit();

  return(0);
}