/* 
 * udpclient.c - A simple UDP client
 * usage: udpclient <host> <port>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <time.h>

#include "binaryStream.h"

#define BUFSIZE 1024

/* 
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(0);
}

int main(int argc, char **argv) {
    int sockfd, portno, n;
    int serverlen;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    char buf[BUFSIZE];

    /* check command line arguments */
    if (argc != 3) {
       fprintf(stderr,"usage: %s <hostname> <port>\n", argv[0]);
       exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);

    /* socket: create the socket */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) 
        error((char *)"ERROR opening socket");

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", hostname);
        exit(0);
    }

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
	  (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);

//////////

    byte_t buffer[MsgData::buffer_size];
    byte_t arrayBuffer[4][MsgData::buffer_size];

	vector<vector<byte_t>> vectorBuffer;
    
    std::cout << "buffer size is " << MsgData::buffer_size << '\n';

    MsgData msgData;
    vector<MsgData> vectorMsgData;
    
    std::cout << "initializing data...";
    msgData.num = 0;
    msgData.num2 = 1;
    msgData.x = 0.0f;
    msgData.elevation = 0;
    msgData.asdf = 'm';
    std::cout << "data is now " << msgData << '\n';
    vectorMsgData.push_back(msgData);
    msgData.num = 11;
    vectorMsgData.push_back(msgData);
    std::cout << " Vector data is now " << vectorMsgData[1] << '\n';
        
    write_msg(buffer, msgData);
        
    write_msg(arrayBuffer[0], vectorMsgData[0]);
//     vectorBuffer.push_back(buffer);
//     write_msg(vectorBuffer[1], vectorMsgData[1]);


    std::cout << "reading data...";
    read_msg(arrayBuffer[0], vectorMsgData[0]);
	
	cout << endl;
    std::cout << "data is now " << vectorMsgData[0] << '\n';
    
//////////


    /* get a message from the user */
    bzero(buf, BUFSIZE);
    printf("Please enter msg: ");
    fgets(buf, BUFSIZE, stdin);

	// Start the timer
// 	clock_t begin, end;
// 	double time_spent;
// 	begin = clock();
// 	time_t beginT;	
// 	struct timeval start, end2;
// 	gettimeofday(&start, NULL);
// 	struct timeval tim;  
//     gettimeofday(&tim, NULL);  
//     double t1=tim.tv_sec+(tim.tv_usec/1000000.0);  
    

// Also casting socketInfo to struct sockaddr * would be nice.
// 
// connect(socketDescriptor,&socketInfo,sizeof(socketInfo));
// Should be
// 
// connect(socketDescriptor,(struct sockaddr *) &socketInfo,sizeof(socketInfo));

    /* send the message to the server */
    serverlen = sizeof(serveraddr);
//     n = sendto(sockfd, buf, strlen(buf), 0, &serveraddr, serverlen);
//     n = sendto(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &serveraddr, serverlen);

	const char* bufferConst;
	bufferConst = reinterpret_cast<const char*>(buffer);
    n = sendto(sockfd, arrayBuffer[0], MsgData::buffer_size, 0, (struct sockaddr *) &serveraddr, serverlen);

    if (n < 0) 
      error((char *)"ERROR in sendto");

	// Stop the timer
// 	end = clock();
// 	time_t endT;
// 	gettimeofday(&end2, NULL);
// 	
// 	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
// 	printf("Time for round trip: %f sec", time_spent);
// 	
// 	double diff = (double)(endT - beginT);
// // 	printf("Time 2 for round trip: %f sec", diff);
// 	
// 	printf("Time 3 for round trip: %ld\n", ((end2.tv_sec * 1000000 + end2.tv_usec)
// 		  - (start.tv_sec * 1000000 + start.tv_usec)));
// 	gettimeofday(&tim, NULL);  
//     double t2=tim.tv_sec+(tim.tv_usec/1000000.0);  
//     printf("Time 4 for round trip: %.6lf seconds elapsed\n", t2-t1);  
	    
    /* print the server's reply */
//     n = recvfrom(sockfd, buf, strlen(buf), 0, &serveraddr, &serverlen);
    n = recvfrom(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &serveraddr,
    	(socklen_t *) &serverlen);
	
    if (n < 0) 
      error((char *)"ERROR in recvfrom");
    printf("Echo from server: %s", buf);
    return 0;
}