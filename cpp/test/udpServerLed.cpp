/* 
 * udpserver.c - A simple UDP echo server 
 * usage: udpserver <port>
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <wiringPi.h>
#include "binaryStream.h"

#define BUFSIZE 1024

/*
 * error - wrapper for perror
 */
void error(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char **argv) {
  int sockfd; /* socket */
  int portno; /* port to listen on */
  int clientlen; /* byte size of client's address */
  struct sockaddr_in serveraddr; /* server's addr */
  struct sockaddr_in clientaddr; /* client addr */
  struct hostent *hostp; /* client host info */
  char buf[BUFSIZE]; /* message buf */
  char *hostaddrp; /* dotted decimal host addr string */
  int optval; /* flag value for setsockopt */
  int n; /* message byte size */

	wiringPiSetup () ;
	pinMode (0, OUTPUT) ;
	pinMode (1, OUTPUT) ;
	pinMode (2, OUTPUT) ;
	pinMode (3, OUTPUT) ;
	
	digitalWrite (0,  LOW);
	digitalWrite (1,  LOW);
	digitalWrite (2,  LOW);
	digitalWrite (3,  LOW);

  /* 
   * check command line arguments 
   */
  if (argc != 2) {
    fprintf(stderr, "usage: %s <port>\n", argv[0]);
    exit(1);
  }
  portno = atoi(argv[1]);

  /* 
   * socket: create the parent socket 
   */
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd < 0) 
    error((char *)"ERROR opening socket");

  /* setsockopt: Handy debugging trick that lets 
   * us rerun the server immediately after we kill it; 
   * otherwise we have to wait about 20 secs. 
   * Eliminates "ERROR on binding: Address already in use" error. 
   */
  optval = 1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, 
	     (const void *)&optval , sizeof(int));

  /*
   * build the server's Internet address
   */
  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short)portno);

  /* 
   * bind: associate the parent socket with a port 
   */
  if (bind(sockfd, (struct sockaddr *) &serveraddr, 
	   sizeof(serveraddr)) < 0) 
    error((char *)"ERROR on binding");

  /* 
   * main loop: wait for a datagram, then echo it
   */
  clientlen = sizeof(clientaddr);
  
  printf("Server is listening on port: %s\n", argv[0]);
  while (1) {

    /*
     * recvfrom: receive a UDP datagram from a client
     */
    bzero(buf, BUFSIZE);
    n = recvfrom(sockfd, buf, BUFSIZE, 0,
		 (struct sockaddr *) &clientaddr, (socklen_t *)&clientlen);
    if (n < 0)
      error((char *)"ERROR in recvfrom");


    printf("server received %d/%d bytes: %s from client %s\n", strlen(buf), n, buf, inet_ntoa(clientaddr.sin_addr));
    
    /////////
   	const unsigned char* bufferConst;
	bufferConst = reinterpret_cast<const unsigned char*>(buf);
	MsgData msgData;
	read_msg(bufferConst, msgData);
	std::cout << "data is now " << msgData << '\n';
    
    ///////////
    
    // if msgData.jsNumber = 0 && msgData.jsValue = 1
    // then set the gpio port 0 to up for 100 msg
    
    if (msgData.jsType == 2 && msgData.jsNumber == 1 && msgData.jsValue < 0) {
// 		printf("Pressed button R2\n");
		digitalWrite(0, HIGH);
// 		delay (100) ;
	} else if (msgData.jsType == 2 && msgData.jsNumber == 1 && msgData.jsValue == 0) {
		digitalWrite (0,  LOW);
		digitalWrite (1,  LOW);
	} else if (msgData.jsType == 2 && msgData.jsNumber == 1 && msgData.jsValue > 0) {
		digitalWrite (1,  HIGH);		
	} else if (msgData.jsType == 2 && msgData.jsNumber == 0 && msgData.jsValue < 0) {
		digitalWrite(3, HIGH);
	} else if (msgData.jsType == 2 && msgData.jsNumber == 0 && msgData.jsValue == 0) {
		digitalWrite (2,  LOW);
		digitalWrite (3,  LOW);
	} else if (msgData.jsType == 2 && msgData.jsNumber == 0 && msgData.jsValue > 0) {
		digitalWrite (2,  HIGH);
	}    
// 	if (msgData.num == 9 && msgData.num2 == 1) {
// 		printf("Pressed button R2\n");
// 
// 		// pew pew
// 		digitalWrite(0, HIGH);
// // 		delay (100) ;
// // 		digitalWrite (0,  LOW);
// 	} else if (msgData.num == 9 && msgData.num2 == 0) {
// 		printf("Pressed button R2\n");
// 
// 		// pew pew
// 		digitalWrite (0,  LOW);
// 	}
    
    //////////
    
    /* 
     * sendto: echo the input back to the client 
     */
    n = sendto(sockfd, buf, strlen(buf), 0, 
	       (struct sockaddr *) &clientaddr, clientlen);
    if (n < 0) 
      error((char *)"ERROR in sendto");

  }
}