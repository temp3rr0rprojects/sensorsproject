#include <stdio.h>
#include <stdint.h>
#include <wiringPi.h>
#include <gertboard.h>

#define SPICLK 18
#define SPIMISO 23
#define SPIMOSI	24
#define	SPICS 25


int pot(adcnum)
{
	uint8_t buffer[3];
    
	buffer[0] = 1;
	buffer[1] = (8+adcnum)<<4;
	buffer[2] = 0;
    
	wiringPiSPIDataRW(0, buffer, 3);
    
	return ((buffer[1]&3) << 8) + buffer[2];
}
int readadc(adcnum)
{
	uint8_t buff[15];
	int adc;
	if ((adcnum > 7) || (adcnum < 0))
		return -1;
	//digitalWrite(SPICS, HIGH);
	//digitalWrite(SPICLK, LOW);
	//digitalWrite(SPICS, LOW);
	
	buff[0] = 1;
	buff[1] = (8 + adcnum)<<4;
	buff[2] = 0;
	wiringPiSPIDataRW(0, buff, 15);
	adc = ((buff[1]&3) << 8) + buff[2];
	return adc;
}

int main(void)
{
	int i, chan;
	uint32_t x1, x3, x0, x7;
	//uint8_t buff[15];

	//pinMode(SPICLK, OUTPUT);
	//pinMode(SPIMISO, INPUT);
	//pinMode(SPICLK, OUTPUT);
	//pinMode(SPICS, OUTPUT);
	//printf("SPI test program\n");
	
	//initialize the WiringPi API
	if (wiringPiSPISetup(0, 100000) < 0 )
		return -1;
	
	//get the channel to read, default to 0
	chan = 0;

	while(1)
	{
		//printf("x1:%d", x1);
		x0 = readadc(0);
		delay(50);
		x1 = readadc(1);
		delay(50);
		x3 = readadc(3);
		delay(50);

		int x4 = readadc(4);
		int x5 = readadc(5);
		int x6 = readadc(6);
		int x2 = readadc(2);
		x7 = readadc(7);
		
		delay(100);
		printf("%d %d %d %d %d %d %d %d\n", x0, x1, x2, x3, x4, x5, x6, x7);
		
		delay(1000);
	}

	return 0;
} // main
