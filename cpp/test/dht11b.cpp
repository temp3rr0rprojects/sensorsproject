#include <string>
#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>

#include "../Sensor.h"
#include "../TempOneWire.h"
#include "../I2C.cpp"
#include "../AnalogSensor.h"
#include "DhtSensor.h"
//#include "../DbController.h"

#define SERVICE     0
#define DEBUG       1
#define TEXT_LOG	1

#define LOG_INTERVAL	3	// Seconds

using namespace std;

void Init(void) {
    system("/home/madks/Documents/CronJobs/modprobe.sh");
    system("/home/madks/Documents/CronJobs/one-wire.sh");
    system("/home/madks/Documents/CronJobs/i2c.sh");
}

int main(void)
{
	
	// Load mods/libraries and start the flow thread	
    Init();
	
	while(1) {    

		DhtSensor dht; 

				cout << "DHT11 Temp: " << dht.getTemp() << endl;
				cout << "DHT11 Hum: " << dht.getHum() << endl;
		
	}
	exit(EXIT_SUCCESS);
}	
