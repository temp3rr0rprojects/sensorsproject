#include <iostream>
#include <tuple>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

typedef unsigned char uint8_t;
typedef unsigned char byte_t;
using namespace std;

struct MsgData {
    int16_t jsNumber;
    int16_t jsValue;
    float x;
    char asdf;
	int16_t jsType;

    static const std::size_t buffer_size = sizeof(int16_t)
    									 + sizeof(int16_t)
                                         + sizeof(float)
                                         + sizeof(char)
					    				 + sizeof(int16_t);

    std::tuple<int16_t&, int16_t&, float&, char&, int16_t&> get_tied_tuple()
    {return std::tie(jsNumber, jsValue, x, asdf, jsType);}
    std::tuple<const int16_t&,const int16_t&, const float&,const char&, const int16_t&> get_tied_tuple() const
    {return std::tie(jsNumber, jsValue, x, asdf, jsType);}
};

// needed only for test output
inline std::ostream& operator<<(std::ostream& os, const MsgData& msgData)
{
    os << '['<< static_cast<int>(msgData.jsType) << ' ' << static_cast<int>(msgData.jsNumber) << ' ' << static_cast<int>(msgData.jsValue) << ' '
       << msgData.x << ' ' << static_cast<char>(msgData.asdf) << ']';
    return os;
}

namespace detail {

    // overload the following two for types that need special treatment
    template<typename T>
    const byte_t* read_value(const byte_t* bin, T& val)
    {
        val = *reinterpret_cast<const T*>(bin);
        return bin + sizeof(T)/sizeof(byte_t);
    }
    template<typename T>
    byte_t* write_value(byte_t* bin, const T& val)
    {
        *reinterpret_cast<T*>(bin) = val;
        return bin + sizeof(T)/sizeof(byte_t);
    }

    template< typename MsgTuple, unsigned int Size = std::tuple_size<MsgTuple>::value >
    struct msg_serializer;

    template< typename MsgTuple >
    struct msg_serializer<MsgTuple,0> {
        static const byte_t* read(const byte_t* bin, MsgTuple&) {return bin;}
        static byte_t* write(byte_t* bin, const MsgTuple&)      {return bin;}
    };

    template< typename MsgTuple, unsigned int Size >
    struct msg_serializer {
        static const byte_t* read(const byte_t* bin, MsgTuple& msg)
        {
            return read_value( msg_serializer<MsgTuple,Size-1>::read(bin, msg)
                             , std::get<Size-1>(msg) );
        }
        static byte_t* write(byte_t* bin, const MsgTuple& msg)
        {
            return write_value( msg_serializer<MsgTuple,Size-1>::write(bin, msg)
                              , std::get<Size-1>(msg) );
        }
    };

    template< class MsgTuple >
    inline const byte_t* do_read_msg(const byte_t* bin, MsgTuple msg)
    {
        return msg_serializer<MsgTuple>::read(bin, msg);
    }

    template< class MsgTuple >
    inline byte_t* do_write_msg(byte_t* bin, const MsgTuple& msg)
    {
        return msg_serializer<MsgTuple>::write(bin, msg);
    }
}

template< class Msg >
inline const byte_t* read_msg(const byte_t* bin, Msg& msg)
{
    return detail::do_read_msg(bin, msg.get_tied_tuple());
}

template< class Msg >
inline const byte_t* write_msg(byte_t* bin, const Msg& msg)
{
    return detail::do_write_msg(bin, msg.get_tied_tuple());
}


