#include <iostream>
#include <thread>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <linux/joystick.h>

#include <wiringPi.h>

using namespace std;

#define NAME_LENGTH 128

void shootLazer(void)
{
	printf("Pressed button R2\n");

	// pew pew
	digitalWrite(0, HIGH);
	delay (100) ;
	digitalWrite (0,  LOW);
}

void playFile(void) {
	// Init
	if (SDL_Init(SDL_INIT_AUDIO) != 0)
		cout << "SDL_Init ERROR: " << SDL_GetError() << endl;
	else
		cout << "SDL_Init SUCCESS" << endl;

	// Open Audio device
	if (Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 2048) != 0)
		cout << "Mix_OpenAudio ERROR: " << Mix_GetError() << endl;
	else
		cout << "Mix_OpenAudio SUCCESS" << endl;

	// Set Volume
	Mix_VolumeMusic(100);

	// Open Audio File
	Mix_Music *music;
	music = Mix_LoadMUS("gunshot.mp3");
	if (music == 0)
		cout << "Mix_LoadMuS ERROR: " << Mix_GetError() << endl;
	else
		cout << "Mix_LoadMus SUCCESS" << endl;

	// Start Playback
	if (Mix_PlayMusic(music, 1) != 0)
		cout << "Mix_PlayMusic ERROR: " << Mix_GetError() << endl;
	else
		cout << "Mix_PlayMusic SUCCESS" << endl;
	
	int startTime = SDL_GetTicks();

	// Wait
	while (Mix_PlayingMusic()) {
		SDL_Delay(1000);
// 		cout << "Time: " << (SDL_GetTicks() - startTime) / 1000 << endl; 
	} 

	// Free File
	Mix_FreeMusic(music);
	music = 0;

	// End
	Mix_CloseAudio();
}

int main() {

	int fd;
	unsigned char axes = 2;
	unsigned char buttons = 2;
	int version = 0x000800;
	char name[NAME_LENGTH] = "Unknown";

	if ((fd = open("/dev/input/js0", O_RDONLY)) < 0) {
		perror("jstest");
		exit(1);
	}
	
	ioctl(fd, JSIOCGVERSION, &version);
	ioctl(fd, JSIOCGAXES, &axes);
	ioctl(fd, JSIOCGBUTTONS, &buttons);
	ioctl(fd, JSIOCGNAME(NAME_LENGTH), name);

	printf("Joystick (%s) has %d axes and %d buttons. Driver version is %d.%d.%d.\n",
		name, axes, buttons, version >> 16, (version >> 8) & 0xff, version & 0xff);
	printf("Testing ... (interrupt to exit)\n");

	struct js_event js;

	wiringPiSetup () ;
	pinMode (0, OUTPUT) ;

	while (1) {
		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			perror("\njstest: error reading");
			exit (1);
		}
		
		if (js.type == 1 && js.value == 1 && js.number == 9) {

				thread shootLazerThread(shootLazer);
				shootLazerThread.join();
				
				thread playSoundThread(playFile);
				playSoundThread.join();
		}

// 		printf("Event: type %d, time %d, number %d, value %d\n",
// 			js.type, js.time, js.number, js.value);


	}
	return -1;
}