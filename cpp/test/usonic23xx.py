#!/usr/bin/python

from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_I2C import Adafruit_I2C
import smbus
import time

mcp = Adafruit_MCP230XX(busnum = 1, address = 0x20, num_gpios = 16)

def reading(sensor):
    import time
    if sensor == 0:
        mcp.config(0, 0)
        mcp.pullup(1, 0)        
        time.sleep(0.5)
        
        mcp.output(0, 0)        
        time.sleep(0.01)
        
        mcp.output(0, 1)        
        time.sleep(0.000001)
        mcp.output(0, 0)
        
        signaloff = time.time()  
        signalon = time.time()        
        
        changed = 0
        while (changed == 0):
			if mcp.input(1) >> 3 != 0:
				changed = 1
				print "Mcp different value: " % mcp.input(1)
        
        while (mcp.input(1) >> 3) == 1:
        	signalon = time.time()
        	print "ok1"
        
        while (mcp.input(1) >> 3) == 0:
        	signalon = time.time()
        	print "ok2"
        
       	timepassed = signalon - signaloff
                
        distance = timepassed * 17000
        
        return distance        

    else:
        print "Incorrect usonic() function varible."

        
print reading(0)
