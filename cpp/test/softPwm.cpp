#include <wiringPi.h>
#include <softPwm.h>

int main() {

	int rounds = 0;
	
	wiringPiSetup();
	
	//int softPwmCreate (int pin, int initialValue, int pwmRange);
	if ((softPwmCreate(0, 0, 100) == 0) && softPwmCreate(1, 0, 100) == 0 && softPwmCreate(2, 0, 100) == 0 && softPwmCreate(3, 0, 100) == 0) {
	
		while(rounds < 2) {
			//void softPwmWrite (int pin, int value);
			softPwmWrite(0, 30);
			delay(2000);
			softPwmWrite(0, 0);
		
			softPwmWrite(1, 30);
			delay(2000);
			softPwmWrite(1, 0);
		
			softPwmWrite(2, 30);
			delay(2000);
			softPwmWrite(2, 0);
		
			softPwmWrite(3, 30);
			delay(2000);
			softPwmWrite(3, 0);
		
			delay(2000);
			rounds++;
		}
	}
	
	return 0;
}
