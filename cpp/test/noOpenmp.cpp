#include <string>
#include <iostream>
#include <vector>
//#include <omp.h>
#include <chrono>
#include <iostream>

using namespace std;
//----------------------------------------------------------
int main(int argc, char* argv[])
{
	volatile int sum = 0;
	volatile float calc = 0.34234234234234;

auto start = chrono::steady_clock::now();
//#ifdef _OPENMP 
//#pragma omp parallel num_threads(4)
//#endif
    	for (; sum < 1000000; sum++)
    	//while (sum < 1000000);
	{
//		int tid = omp_get_thread_num();
      		//cout << "t:" << tid;
		//cout << " sum: " << sum << endl;
		calc = ((sum ^ 3) / 1.2342) * (sum * 3.43234) ;
    	
	}
auto end = chrono::steady_clock::now();

auto diff = end - start;

cout << "Diff: " << diff.count() << endl;
cout << "Calc: " << calc << endl;
	return 0;
}
