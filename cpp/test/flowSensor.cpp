#include <string>
#include <iostream>
#include <thread>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "../Sensor.h"
#include "../TempOneWire.h"
#include "../I2C.cpp"
#include "../AnalogSensor.h"
#include "../DhtSensor.h"
#include "../FlowSensor.h"
#include "../DbController.h"
#include "../ThermalZone.h"
#include "../CpuFrequency.h"
#include "../QueryLogger.h"
#include "../TextLogger.h"

#define SERVICE     0
#define DEBUG       1
#define TEXT_LOG	1

#define LOG_INTERVAL	3	// Seconds

using namespace std;

static volatile int flowCounter = 0;
static volatile int flowPulses = 0;

void flowInterrupt(void) {
    ++flowCounter;
}

void flowLogThread(void) {
	int pulses = 0;
    wiringPiSetup();
    wiringPiISR(0, INT_EDGE_FALLING, &flowInterrupt);
    std:thread flowThread(flowInterrupt);
    flowThread.join();
    
    auto start = chrono::system_clock::now();
    sleep(2);
    pulses = flowCounter;
    auto end = chrono::system_clock::now();
    flowCounter = 0;

    auto diff = end - start;
    float flDiff = chrono::duration<double, milli>(diff).count();
    float hz = pulses / (flDiff / 1000);
    float liters = pulses;
    liters /= 7.5;
    liters /= 60.0;
	flowPulses = pulses;
}

void Init(void) {
    system("/home/madks/Documents/CronJobs/modprobe.sh");
    system("/home/madks/Documents/CronJobs/one-wire.sh");
    system("/home/madks/Documents/CronJobs/i2c.sh");
}

void getFlowReading(void) {
	// Get Flow reading
	thread flowThread(flowLogThread);
	flowThread.join();
}

int main(void)
{
	// Load mods/libraries and start the flow thread	
    Init();
	
	while(1) {    
		// Instantiate Sensors    
		FlowSensor fl;	
	
		getFlowReading();	
		fl.setFlow(flowPulses);
		cout << flowPulses << endl;
		cout << "fl = " << fl.getFlow() << endl;
			

		sleep(LOG_INTERVAL);
	}
	exit(EXIT_SUCCESS);
}	
