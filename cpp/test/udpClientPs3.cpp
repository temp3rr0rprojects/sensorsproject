#include <iostream>
#include <thread>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <linux/joystick.h>


using namespace std;

#define NAME_LENGTH 128

#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <time.h>

#include "binaryStream.h"

#define BUFSIZE 1024

int gArgc = 0;
char **gArgv;

/* 
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(0);
}

void shootLazer(int type, int button, int value)
{
	printf("Pressed button R2\n");

	int sockfd, portno, n;
	int serverlen;
	struct sockaddr_in serveraddr;
	struct hostent *server;
	char *hostname;
	char buf[BUFSIZE];

	/* check command line arguments */
	if (gArgc != 3) {
	   fprintf(stderr,"usage: %s <hostname> <port>\n", gArgv[0]);
	   exit(0);
	}
	hostname = gArgv[1];
	portno = atoi(gArgv[2]);

	/* socket: create the socket */
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) 
		error((char *)"ERROR opening socket");

	/* gethostbyname: get the server's DNS entry */
	server = gethostbyname(hostname);
	if (server == NULL) {
		fprintf(stderr,"ERROR, no such host as %s\n", hostname);
		exit(0);
	}

	/* build the server's Internet address */
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, 
	  (char *)&serveraddr.sin_addr.s_addr, server->h_length);
	serveraddr.sin_port = htons(portno);

//////////

	byte_t buffer[MsgData::buffer_size];
	byte_t arrayBuffer[4][MsgData::buffer_size];

	vector<vector<byte_t>> vectorBuffer;

	std::cout << "buffer size is " << MsgData::buffer_size << '\n';

	MsgData msgData;
	vector<MsgData> vectorMsgData;

	std::cout << "initializing data...";
	msgData.jsType = type;
	msgData.jsNumber = button;
	msgData.jsValue = value;
	msgData.x = 0.0f;
	msgData.asdf = 'm';
	std::cout << "data is now " << msgData << '\n';
	vectorMsgData.push_back(msgData);
	msgData.jsNumber = 11;
	vectorMsgData.push_back(msgData);
	std::cout << " Vector data is now " << vectorMsgData[1] << '\n';

	write_msg(buffer, msgData);

	write_msg(arrayBuffer[0], vectorMsgData[0]);
//     vectorBuffer.push_back(buffer);
//     write_msg(vectorBuffer[1], vectorMsgData[1]);


	std::cout << "reading data...";
	read_msg(arrayBuffer[0], vectorMsgData[0]);

	cout << endl;
	std::cout << "data is now " << vectorMsgData[0] << '\n';

//////////


	/* get a message from the user */
	bzero(buf, BUFSIZE);
//     printf("Please enter msg: ");
//     fgets(buf, BUFSIZE, stdin);

	// Start the timer
// 	clock_t begin, end;
// 	double time_spent;
// 	begin = clock();
// 	time_t beginT;	
// 	struct timeval start, end2;
// 	gettimeofday(&start, NULL);
// 	struct timeval tim;  
//     gettimeofday(&tim, NULL);  
//     double t1=tim.tv_sec+(tim.tv_usec/1000000.0);  


// Also casting socketInfo to struct sockaddr * would be nice.
// 
// connect(socketDescriptor,&socketInfo,sizeof(socketInfo));
// Should be
// 
// connect(socketDescriptor,(struct sockaddr *) &socketInfo,sizeof(socketInfo));

	/* send the message to the server */
	serverlen = sizeof(serveraddr);
//     n = sendto(sockfd, buf, strlen(buf), 0, &serveraddr, serverlen);
//     n = sendto(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &serveraddr, serverlen);

	const char* bufferConst;
	bufferConst = reinterpret_cast<const char*>(buffer);
	n = sendto(sockfd, arrayBuffer[0], MsgData::buffer_size, 0, (struct sockaddr *) &serveraddr, serverlen);

	if (n < 0) 
	  error((char *)"ERROR in sendto");

	// Stop the timer
// 	end = clock();
// 	time_t endT;
// 	gettimeofday(&end2, NULL);
// 	
// 	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
// 	printf("Time for round trip: %f sec", time_spent);
// 	
// 	double diff = (double)(endT - beginT);
// // 	printf("Time 2 for round trip: %f sec", diff);
// 	
// 	printf("Time 3 for round trip: %ld\n", ((end2.tv_sec * 1000000 + end2.tv_usec)
// 		  - (start.tv_sec * 1000000 + start.tv_usec)));
// 	gettimeofday(&tim, NULL);  
//     double t2=tim.tv_sec+(tim.tv_usec/1000000.0);  
//     printf("Time 4 for round trip: %.6lf seconds elapsed\n", t2-t1);  

	/* print the server's reply */
//     n = recvfrom(sockfd, buf, strlen(buf), 0, &serveraddr, &serverlen);
	n = recvfrom(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &serveraddr,
		(socklen_t *) &serverlen);

	if (n < 0) 
	  error((char *)"ERROR in recvfrom");
	printf("Echo from server: %s", buf);
	
	close(sockfd);
}

void playFile(void) {
	// Init
	if (SDL_Init(SDL_INIT_AUDIO) != 0)
		cout << "SDL_Init ERROR: " << SDL_GetError() << endl;
	else
		cout << "SDL_Init SUCCESS" << endl;

	// Open Audio device
	if (Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 2048) != 0)
		cout << "Mix_OpenAudio ERROR: " << Mix_GetError() << endl;
	else
		cout << "Mix_OpenAudio SUCCESS" << endl;

	// Set Volume
	Mix_VolumeMusic(100);

	// Open Audio File
	Mix_Music *music;
	music = Mix_LoadMUS("gunshot.mp3");
	if (music == 0)
		cout << "Mix_LoadMuS ERROR: " << Mix_GetError() << endl;
	else
		cout << "Mix_LoadMus SUCCESS" << endl;

	// Start Playback
	if (Mix_PlayMusic(music, 1) != 0)
		cout << "Mix_PlayMusic ERROR: " << Mix_GetError() << endl;
	else
		cout << "Mix_PlayMusic SUCCESS" << endl;
	
	int startTime = SDL_GetTicks();

	// Wait
	while (Mix_PlayingMusic()) {
		SDL_Delay(1000);
// 		cout << "Time: " << (SDL_GetTicks() - startTime) / 1000 << endl; 
	} 

	// Free File
	Mix_FreeMusic(music);
	music = 0;

	// End
	Mix_CloseAudio();
}


int main(int argc, char **argv) {

	int fd;
	unsigned char axes = 2;
	unsigned char buttons = 2;
	int version = 0x000800;
	char name[NAME_LENGTH] = "Unknown";
	gArgc = argc;
	gArgv = argv;

	if ((fd = open("/dev/input/js0", O_RDONLY)) < 0) {
		perror("jsValue");
		exit(1);
	}
	
	ioctl(fd, JSIOCGVERSION, &version);
	ioctl(fd, JSIOCGAXES, &axes);
	ioctl(fd, JSIOCGBUTTONS, &buttons);
	ioctl(fd, JSIOCGNAME(NAME_LENGTH), name);

	printf("Joystick (%s) has %d axes and %d buttons. Driver version is %d.%d.%d.\n",
		name, axes, buttons, version >> 16, (version >> 8) & 0xff, version & 0xff);
	printf("Testing ... (interrupt to exit)\n");

	struct js_event js;

	// Blocking mode
// 	while (1) {
// 		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
// 			perror("\njsValue: error reading");
// 			exit (1);
// 		}		
// 		if (js.type == 1 && js.value == 1 && js.number == 9) {
// 			shootLazer(argc, argv);
// 		}
// 	}
	
	
	// Non Blocking mode
	while (1) {
		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			perror("\njsValue: error reading");
			exit (1);
		}
				
		if (js.type == 1 && js.value == 1 && js.number == 9) {
			shootLazer(js.type, js.number, js.value);
			printf("R2 start\n");
		} else if (js.type == 1 && js.value == 0 && js.number == 9) {
			shootLazer(js.type, js.number, js.value);
			printf("R2 stop\n");			
		}
		
		if (js.type == 2 && js.value < 0 && js.number == 1) {
			shootLazer(js.type, js.number, js.value);
			printf("Axis X Up\n");
		} else if (js.type == 2 && js.value > 0 && js.number == 1) {
			shootLazer(js.type, js.number, js.value);
			printf("Axis X Down\n");
		} else if (js.type == 2 && js.value == 0 && js.number == 1) {
			shootLazer(js.type, js.number, js.value);
			printf("Axis X Center\n");			
		} else if (js.type == 2 && js.value > 0 && js.number == 0) {
			shootLazer(js.type, js.number, js.value);
			printf("Axis Y Right\n");
		} else if (js.type == 2 && js.value == 0 && js.number == 0) {
			shootLazer(js.type, js.number, js.value);
			printf("Axis Y Center\n");			
		} else if (js.type == 2 && js.value < 0 && js.number == 0) {
			shootLazer(js.type, js.number, js.value);
			printf("Axis Y Left\n");			
		}
		
	}

// 	while (1) {
// 		while (read(fd, &js, sizeof(struct js_event)) == sizeof(struct js_event))  {
// 			if (js.type == 1 && js.value == 1 && js.number == 9) {
// // 				shootLazer(argc, argv);
// 				printf("R2 pressed");
// 				sleep(50);
// 				read(fd, &js, sizeof(struct js_event));
// 			}
// 		}
// 		if (errno != EAGAIN) {
// 			perror("\njsValue: error reading");
// 			exit (1);
// 		}
// 		usleep(10000);
// 	}

	return -1;
}