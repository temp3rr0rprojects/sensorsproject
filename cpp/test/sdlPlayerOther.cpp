#include <iostream>
#include <thread>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

using namespace std;

void playFile(void) {
	// Init
	if (SDL_Init(SDL_INIT_AUDIO) != 0)
		cout << "SDL_Init ERROR: " << SDL_GetError() << endl;
	else
		cout << "SDL_Init SUCCESS" << endl;

	// Open Audio device
	if (Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 2048) != 0)
		cout << "Mix_OpenAudio ERROR: " << Mix_GetError() << endl;
	else
		cout << "Mix_OpenAudio SUCCESS" << endl;

	// Set Volume
	Mix_VolumeMusic(100);

	// Open Audio File
	Mix_Music *music;
	music = Mix_LoadMUS("gunshot.mp3");
	if (music == 0)
		cout << "Mix_LoadMuS ERROR: " << Mix_GetError() << endl;
	else
		cout << "Mix_LoadMus SUCCESS" << endl;

	// Start Playback
	if (Mix_PlayMusic(music, 1) != 0)
		cout << "Mix_PlayMusic ERROR: " << Mix_GetError() << endl;
	else
		cout << "Mix_PlayMusic SUCCESS" << endl;
	
	int startTime = SDL_GetTicks();

	// Wait
	while (Mix_PlayingMusic()) {
		SDL_Delay(1000);
// 		cout << "Time: " << (SDL_GetTicks() - startTime) / 1000 << endl; 
	} 

	// Free File
	Mix_FreeMusic(music);
	music = 0;

	// End
	Mix_CloseAudio();
}

int main() {

	thread playSoundThread(playFile);
	playSoundThread.join();
	
// 	playFile();
	
	return 0;
}