#include <string>
#include <iostream>
#include <thread>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "Sensor.h"
#include "TempOneWire.h"
#include "I2C.cpp"
#include "AnalogSensor.h"
#include "DhtSensor.h"
#include "FlowSensor.h"
#include "DbController.h"
#include "ThermalZone.h"
#include "CpuFrequency.h"
#include "QueryLogger.h"
#include "TextLogger.h"

#define SERVICE     1
#define DEBUG       0
#define TEXT_LOG	1

#define LOG_INTERVAL	1800	// Seconds

using namespace std;

static volatile int flowCounter = 0;
static volatile int flowPulses = 0;

void flowInterrupt(void) {
    ++flowCounter;
}

void flowLogThread(void) {
	int pulses = 0;
    wiringPiSetup();
    wiringPiISR(0, INT_EDGE_FALLING, &flowInterrupt);
    std:thread flowThread(flowInterrupt);
    flowThread.join();
    
    auto start = chrono::system_clock::now();
    sleep(1);
    pulses = flowCounter;
    auto end = chrono::system_clock::now();
    flowCounter = 0;

    auto diff = end - start;
    float flDiff = chrono::duration<double, milli>(diff).count();
    float hz = pulses / (flDiff / 1000);
    float liters = pulses;
    liters /= 7.5;
    liters /= 60.0;
	flowPulses = pulses;
}

void Init(void) {
    system("/home/madks/Documents/CronJobs/modprobe.sh");
    system("/home/madks/Documents/CronJobs/one-wire.sh");
    system("/home/madks/Documents/CronJobs/i2c.sh");
}

void getFlowReading(void) {
	// Get Flow reading
	thread flowThread(flowLogThread);
	flowThread.join();
}

void makeService(void) {
	// Our process ID and Session ID
	pid_t pid, sid;
		
	// Fork off the parent process
	pid = fork();
	if (pid < 0)
		exit(EXIT_FAILURE);
	
	// If we got a good PID, then we can exit the parent process
	if (pid > 0)
		exit(EXIT_SUCCESS);

	// Change the file mode mask
	umask(0);
		   
	// Create a new SID for the child process
	sid = setsid();
	if (sid < 0) {
			// Log the failure
			exit(EXIT_FAILURE);
	}
	
	// Change the current working directory
	if ((chdir("/")) < 0) {
			// Log the failure
			exit(EXIT_FAILURE);
	}
	
	// Close out the standard file descriptors
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	// Daemon-specific initialization goes here
}

int main(void)
{
	if (SERVICE)
		makeService();
	
	// Load mods/libraries and start the flow thread	
    Init();
	
	while(1) {    
		// Instantiate Sensors
		TempOneWire digitalTemp;
		I2C mpl;
		AnalogSensor logTemp;
		DhtSensor dht;    
		FlowSensor fl;
		ThermalZone thermal;
		CpuFrequency cpuFreq;		
	
		// Instantiate Controllers
		DbController myController;	
		QueryLogger *ql;
		TextLogger *tx;

		// Instantiate sensors vector, flow sensor and the loggers
		vector<Sensor*> sensorsVector;
		ql = QueryLogger::getInstance();
		tx = TextLogger::getInstance();

		try {	
			// Attach sensors to the vectors
			sensorsVector.push_back(&digitalTemp);
			sensorsVector.push_back(&mpl);
			sensorsVector.push_back(&logTemp);
			sensorsVector.push_back(&dht);
			sensorsVector.push_back(&fl);
			sensorsVector.push_back(&thermal);
			sensorsVector.push_back(&cpuFreq);
		} catch (const std::out_of_range& oor) {
			if(TEXT_LOG)
				tx->log(oor.what());
		}
		
		// Declare custom type vectors
		vector<tuple<Sensor*, string>> i2cVector, digitalVector, analogVector, localVector;
	
		if(TEXT_LOG)
			tx->log("Init.");

		bool dbConnected = true; // Initiate db connection check		
		if (myController.connect()) {
			// todo: check auto
			try {	
				getFlowReading();	
				fl.setFlow(flowPulses);
				if (DEBUG) {
					cout << flowPulses << endl;
					cout << "fl = " << fl.getFlow() << endl;
					if (TEXT_LOG) {
						string message = "Flow Pulses: ";
						message.append(to_string(flowPulses));
						tx->log(message);
					}
					cout << logTemp.getValue() << endl;
				}
						
				// Populate each custom type sensor vector
				string name, id0;
				int j = 0, loops = 1;
				
				for (int i = 0; i < sensorsVector.size(); i++) {				
					try {
						j = 0;				
						name = sensorsVector.at(i)->getName();
						if (name == "I2C" || name == "DhtSensor")
							loops = 2;
						else 
							loops = 1;
					
						while (j < loops) {
							id0 = myController.select(sensorsVector.at(i)->getId(j), sensorsVector.at(i)->getTable(j), sensorsVector.at(i)->getField(j), sensorsVector.at(i)->getValue(j));			
							if (id0 == "0")
								id0 = myController.insert(sensorsVector.at(i)->getTable(j), sensorsVector.at(i)->getField(j), sensorsVector.at(i)->getValue(j));			
				
							if (name ==  "I2C")
								i2cVector.push_back(make_tuple(sensorsVector.at(i), id0));
							else if (name == "TempOneWire" || name == "DhtSensor")
								digitalVector.push_back(make_tuple(sensorsVector.at(i), id0));
							else if (name == "ThermalZone" || name == "CpuFrequency")
								localVector.push_back(make_tuple(sensorsVector.at(i), id0));
							else
								analogVector.push_back(make_tuple(sensorsVector.at(i), id0));
						
							j++;
						}					
					} catch (const std::out_of_range& oor) {
						if(TEXT_LOG)
							tx->log(oor.what());
					}
				}
			
				// Vector that helps in the last query
				vector<tuple<string, string>> id2;
				
				try {
					// Intermediate queries
					id2 = ql->level2(digitalVector, myController, id2);	// Digital query								
					id2 = ql->level2(analogVector, myController, id2);	// Analog query			
					id2 = ql->level2(localVector, myController, id2);	// Local querys			
					id2 = ql->level2(i2cVector, myController, id2);	// I2C query
				} catch (const std::exception &exc) {
					if(TEXT_LOG)
						tx->log(exc.what());
				}
				
				if(DEBUG)
					cout << "Pre part 3" << endl;

				// Last query for Log table
				ql->level3(id2, myController);
			} catch (const std::out_of_range& oor) {
				if(TEXT_LOG)
					tx->log(oor.what());
			} catch (const std::exception& exc) {
				if(TEXT_LOG)
					tx->log(exc.what());
			}
		}
		else
			dbConnected = false;
			
		if(dbConnected)
			myController.close();
		else {
			if(TEXT_LOG)
				tx->log("Db was disconnected.");
		}
				
		sleep(LOG_INTERVAL);
	}
	exit(EXIT_SUCCESS);
}	
