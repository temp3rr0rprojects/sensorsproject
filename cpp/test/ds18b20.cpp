#include <string>
#include <iostream>
#include <thread>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "../Sensor.h"
#include "../TempOneWire.h"
#include "../I2C.cpp"
#include "../AnalogSensor.h"
#include "../DhtSensor.h"
#include "../FlowSensor.h"
#include "../DbController.h"
#include "../ThermalZone.h"
#include "../CpuFrequency.h"
#include "../QueryLogger.h"
#include "../TextLogger.h"

#define SERVICE     0
#define DEBUG       1
#define TEXT_LOG	1

#define LOG_INTERVAL	3	// Seconds

using namespace std;

void Init(void) {
    system("/home/madks/Documents/CronJobs/modprobe.sh");
    system("/home/madks/Documents/CronJobs/one-wire.sh");
    system("/home/madks/Documents/CronJobs/i2c.sh");
}

int main(void)
{
    Init();
	
	while(1) {    
		// Instantiate Sensors
		TempOneWire digitalTemp;

		cout << "Digital Temp: " << digitalTemp.getTemp() << endl;
		sleep(LOG_INTERVAL);
	}
	exit(EXIT_SUCCESS);
}	
