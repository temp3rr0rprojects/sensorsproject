from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_I2C import Adafruit_I2C
import smbus
import RPi.GPIO as GPIO
import time

mcp = Adafruit_MCP230XX(busnum = 1, address = 0x20, num_gpios = 16)

GPIO.setmode(GPIO.BCM)
PIR_PIN = 4

mcp.pullup(0, 0)

GPIO.setup(PIR_PIN, GPIO.IN)

def MOTION(PIR_PIN):
	print "Motion Detected"

	print "PIR Module Test (CTRL+C to exit)"

	time.sleep(2)

	print "Ready"

try:
	GPIO.add_event_detect(PIR_PIN, GPIO.RISING, callback=MOTION)
	while 1:
		time.sleep(100)
except KeyboardInterrupt:
	print " Quit"
	GPIO.cleanup()
