#include <string>
#include <thread>

#include <iostream>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <linux/joystick.h>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#include "binaryStream.h"

#include <wiringPi.h>
#include <softPwm.h>

using namespace std;

#define NAME_LENGTH 128

#define BUFSIZE 1024

#define SERVICE	0
#define DEBUG	1

int gArgc = 0;
char **gArgv;

volatile int axisX = 0;
volatile int axisY = 0;

// Gets the level of reduction on the left
// or right controller in order to move
// to the corresponding side
int getReducedSpeed(int speed, int counterAxisX) {
	int returningValue = 0;
	
	// Get the absolute value of the axisX
	if (counterAxisX < 0)
		counterAxisX *= -1;
	
	// Get the percentage of reduction (0.01 .. 0.99)
	returningValue = speed - (counterAxisX / 327);
	
	if (returningValue > 100) returningValue = 100;
	if (returningValue < 0) returningValue = 0;

	float returningValueFloat = (float)returningValue / 100.0;
	
	returningValue = (int)((float)speed * returningValueFloat);

	if (returningValue > 100) returningValue = 100;
	if (returningValue < 0) returningValue = 0;

	return returningValue;
}

// Converts the -32767 .. 32767 to a value 0 .. 100
int getSpeed(int reading) {
	int returningValue = 0;
	
	if (reading < 0)
		reading *= -1;
	
	returningValue = (reading / 327);
	
	if (returningValue > 100) returningValue = 100;
	if (returningValue < 0) returningValue = 0;
	
	return returningValue;
}


// This will be called by thread
// Applies the current x, y values of
// the analog sticks as combinations of OUT1,
// OUT2, OUT3, OUT4
void dcMotorPwm(void) {

// 	if (DEBUG)
// 		cout << "Dc Motor Pwm thread started" << endl;
// 	
	// Debug message
	if (DEBUG) {
		cout << "AxisX: " << axisX << " AxisY: " << axisY << endl;
	}

	// Move forwards, backwards or stay in the same place
	if (axisY == 0) {
		// Kill the motors
		softPwmWrite(0, 0);
		softPwmWrite(1, 0);
		softPwmWrite(2, 0);
		softPwmWrite(3, 0);			
	} else {
	
		// Calculate the absolute speed 
		int fullPwmSpeed = getSpeed(axisY);
	
		int motorLeft = 0;
		int motorRight = 0;
	
		// Forward
		if (axisY < 0) {
			motorLeft = 0;
			motorRight = 2;
		} else {
			motorLeft = 1;
			motorRight = 3;
		}
		
		int reducedPwmSpeed = fullPwmSpeed;
		
		if (axisX != 0) {
			// Calculate the percentage of reduction
			reducedPwmSpeed = getReducedSpeed(fullPwmSpeed, axisX);
		}
		
		if (axisX < 0) {
			// Go right
			softPwmWrite(motorLeft, reducedPwmSpeed);
			softPwmWrite(motorRight, fullPwmSpeed);
		}
		else {
			// Go left
			softPwmWrite(motorRight, reducedPwmSpeed);
			softPwmWrite(motorLeft, fullPwmSpeed);
		}
	
		// Debug message
		if (DEBUG) {
			cout << "ReducedSpeed: " << reducedPwmSpeed
				<< " FullSpeed: " << fullPwmSpeed << endl;
		}

		// if (DEBUG) {
// 			cout << "AxisX: " << axisX << " AxisY: " << axisY << " ReducedSpeed: " << reducedPwmSpeed
// 				<< " FullSpeed: " << fullPwmSpeed << " MotorRight: " << motorRight << " MotorLeft: " << motorLeft << endl;
// 		}
	}

	// Wait 10ms
	delay(10);
}

/* 
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(0);
}

void makeService(void) {
	// Our process ID and Session ID
	pid_t pid, sid;
		
	// Fork off the parent process
	pid = fork();
	if (pid < 0)
		exit(EXIT_FAILURE);
	
	// If we got a good PID, then we can exit the parent process
	if (pid > 0)
		exit(EXIT_SUCCESS);

	// Change the file mode mask
	umask(0);
		   
	// Create a new SID for the child process
	sid = setsid();
	if (sid < 0) {
			// Log the failure
			exit(EXIT_FAILURE);
	}
	
	// Change the current working directory
	if ((chdir("/")) < 0) {
			// Log the failure
			exit(EXIT_FAILURE);
	}
	
	// Close out the standard file descriptors
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	// Daemon-specific initialization goes here
}

void setAxis(int type, int button, int value) {
	
	if(DEBUG)
		cout << "Setting axis" << endl;
	
	if (type == 2 && button == 1) {	
		// Front - Backwards
		axisY = value;
	} else 	if (type == 2 && button == 2) {	
		// Left - Right
		axisX = value;	
	} else 	if ((type == 2 && value == 0 && (button == 1 || button == 2)) || button == 14) {	
		// Center
		if (button == 1)
			axisY = 0;
		if (button == 3)
			axisX = 0;
	}
}

void startPwmChecks(void) {
	thread dcMotorPwmThread(dcMotorPwm);
	dcMotorPwmThread.join();
}

// int main(int argc, char **argv) {
// 
// 	int fd;
// 	unsigned char axes = 2;
// 	unsigned char buttons = 2;
// 	int version = 0x000800;
// 	char name[NAME_LENGTH] = "Unknown";
// 	gArgc = argc;
// 	gArgv = argv;
// 
// 	if ((fd = open("/dev/input/js0", O_RDONLY)) < 0) {
// 		perror("jsValue");
// 		exit(1);
// 	}
// 	
// 	wiringPiSetup();
// 
// 	if (!((softPwmCreate(0, 0, 100) == 0) && softPwmCreate(1, 0, 100) == 0 && softPwmCreate(2, 0, 100) == 0 && softPwmCreate(3, 0, 100) == 0)) {
// 		perror("softwarePwm");
// 		exit(1);
// 	}
// 	
// 	ioctl(fd, JSIOCGVERSION, &version);
// 	ioctl(fd, JSIOCGAXES, &axes);
// 	ioctl(fd, JSIOCGBUTTONS, &buttons);
// 	ioctl(fd, JSIOCGNAME(NAME_LENGTH), name);
// 
// 	printf("Joystick (%s) has %d axes and %d buttons. Driver version is %d.%d.%d.\n",
// 		name, axes, buttons, version >> 16, (version >> 8) & 0xff, version & 0xff);
// 	printf("Testing ... (interrupt to exit)\n");
// 
// 	struct js_event js;
// 	
// 	if (SERVICE)
// 		makeService();
// 	
// 	if (DEBUG)
// 		cout << "Reading controller values..." << endl;
// 	
// 	bool started = false;
// 	
// 	// Non Blocking mode
// 	while (1) {
// 		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
// 			perror("\njsValue: error reading");
// 			exit (1);
// 		}
// 		
// 		setAxis(js.type, js.number, js.value);
// 				
// 		dcMotorPwm();			
// 	}
// 
// 	return -1;
// }
int main(int argc, char **argv) {
  int sockfd; /* socket */
  int portno; /* port to listen on */
  int clientlen; /* byte size of client's address */
  struct sockaddr_in serveraddr; /* server's addr */
  struct sockaddr_in clientaddr; /* client addr */
  struct hostent *hostp; /* client host info */
  char buf[BUFSIZE]; /* message buf */
  char *hostaddrp; /* dotted decimal host addr string */
  int optval; /* flag value for setsockopt */
  int n; /* message byte size */

	cout << "Starting udp 360 pwm server" << endl;

  /* 
   * check command line arguments 
   */
  if (argc != 2) {
    fprintf(stderr, "usage: %s <port>\n", argv[0]);
    exit(1);
  }
  portno = atoi(argv[1]);

  /* 
   * socket: create the parent socket 
   */
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd < 0) 
    error((char *)"ERROR opening socket");

  /* setsockopt: Handy debugging trick that lets 
   * us rerun the server immediately after we kill it; 
   * otherwise we have to wait about 20 secs. 
   * Eliminates "ERROR on binding: Address already in use" error. 
   */
  optval = 1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, 
	     (const void *)&optval , sizeof(int));

  /*
   * build the server's Internet address
   */
  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short)portno);

  /* 
   * bind: associate the parent socket with a port 
   */
  if (bind(sockfd, (struct sockaddr *) &serveraddr, 
	   sizeof(serveraddr)) < 0) 
    error((char *)"ERROR on binding");

	wiringPiSetup();

	if (!((softPwmCreate(0, 0, 100) == 0) && softPwmCreate(1, 0, 100) == 0 && softPwmCreate(2, 0, 100) == 0 && softPwmCreate(3, 0, 100) == 0)) {
		perror("softwarePwm");
		exit(1);
	}

  /* 
   * main loop: wait for a datagram, then echo it
   */
  clientlen = sizeof(clientaddr);
  while (1) {

    /*
     * recvfrom: receive a UDP datagram from a client
     */
    bzero(buf, BUFSIZE);
    n = recvfrom(sockfd, buf, BUFSIZE, 0,
		 (struct sockaddr *) &clientaddr, (socklen_t *)&clientlen);
    if (n < 0)
      error((char *)"ERROR in recvfrom");

    /* 
     * gethostbyaddr: determine who sent the datagram
     */
//     hostp = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, 
// 			  sizeof(clientaddr.sin_addr.s_addr), AF_INET);
//     if (hostp == NULL)
//       error("ERROR on gethostbyaddr");
// 
//     
//     hostaddrp = inet_ntoa(clientaddr.sin_addr);
//     if (hostaddrp == NULL)
//       error("ERROR on inet_ntoa\n");
//       
//     printf("server received datagram from %s (%s)\n", 
// 	   hostp->h_name);
    printf("server received %d/%d bytes: %s from client %s\n", strlen(buf), n, buf, inet_ntoa(clientaddr.sin_addr));
    
    /////////
   	const unsigned char* bufferConst;
	bufferConst = reinterpret_cast<const unsigned char*>(buf);
	MsgData msgData;
	read_msg(bufferConst, msgData);
	std::cout << "data is now " << msgData << '\n';
    

    ///////////
    
    /* 
     * sendto: echo the input back to the client 
     */
//     n = sendto(sockfd, buf, strlen(buf), 0, 
// 	       (struct sockaddr *) &clientaddr, clientlen);
    if (n < 0) 
      error((char *)"ERROR in sendto");

	cout << "Starting pwm" << endl;      
    // send forward 50
    
    setAxis(msgData.jsType , msgData.jsNumber, msgData.jsValue);    
	dcMotorPwm();
	
//  	sleep(1);
// 	usleep(100000);
	
// 	setAxis(2, 1, 0);
// 	dcMotorPwm();
	
	cout << "Ending pwm" << endl;      
     
  }
}