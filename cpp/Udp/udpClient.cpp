/* 
 * udpclient.c - A simple UDP client
 * usage: udpclient <host> <port>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <time.h>

#include "binaryStream.h"

#define BUFSIZE 1024

/* 
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(0);
}

int main(int argc, char **argv) {
    int sockfd, portno, n;
    int serverlen;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    char buf[BUFSIZE];

    /* check command line arguments */
    if (argc != 3) {
       fprintf(stderr,"usage: %s <hostname> <port>\n", argv[0]);
       exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);

    /* socket: create the socket */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) 
        error((char *)"ERROR opening socket");

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", hostname);
        exit(0);
    }

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
	  (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);

//////////

    byte_t buffer[MsgData::buffer_size];
    byte_t arrayBuffer[4][MsgData::buffer_size];

	vector<vector<byte_t>> vectorBuffer;
    
    std::cout << "buffer size is " << MsgData::buffer_size << '\n';

    MsgData msgData;
    vector<MsgData> vectorMsgData;
    
    std::cout << "initializing data...";
    
    msgData.jsType = 2;
    msgData.jsNumber = 1;
    msgData.jsValue = 30000;
    
    std::cout << "data is now " << msgData << '\n';
    vectorMsgData.push_back(msgData);
    //msgData.jsValue = 30000;
    vectorMsgData.push_back(msgData);
    std::cout << " Vector data is now " << vectorMsgData[1] << '\n';
        
    write_msg(buffer, msgData);
    write_msg(arrayBuffer[0], vectorMsgData[0]);

    std::cout << "reading data...";
    read_msg(arrayBuffer[0], vectorMsgData[0]);
	
	cout << endl;
    std::cout << "data is now " << vectorMsgData[0] << '\n';

    /* get a message from the user */
    bzero(buf, BUFSIZE);
    printf("Please enter msg: ");
    fgets(buf, BUFSIZE, stdin);

    /* send the message to the server */
    serverlen = sizeof(serveraddr);

	const char* bufferConst;
	bufferConst = reinterpret_cast<const char*>(buffer);
    n = sendto(sockfd, arrayBuffer[0], MsgData::buffer_size, 0, (struct sockaddr *) &serveraddr, serverlen);

    if (n < 0) 
      error((char *)"ERROR in sendto");

    /* print the server's reply */
    n = recvfrom(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &serveraddr,
    	(socklen_t *) &serverlen);
	
    if (n < 0) 
      error((char *)"ERROR in recvfrom");
    printf("Echo from server: %s", buf);
    return 0;
}